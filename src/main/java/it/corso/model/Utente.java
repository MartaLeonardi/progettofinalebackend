package it.corso.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;

@Entity
@Table( name = "utente" )
public class Utente {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "id_u" )
	private int idUtente;
	
	@Column( name = "nome" )
	private String nome;
	
	@Column( name = "cognome" )
	private String cognome;
	
	@Column( name = "email" )
	private String email;
	
	@Column( name = "password" )
	private String password;
	
	//COLLEGAMENTO TRA TABELLA UTENTE E LA TABELLA RUOLI
	@ManyToMany( cascade = CascadeType.REFRESH, fetch = FetchType.EAGER )
	@JoinTable
	(
			name = "utente_ruolo", 
			joinColumns = @JoinColumn( name = "fk_u" , referencedColumnName = "id_u" ),
			inverseJoinColumns = @JoinColumn( name = "fk_r", referencedColumnName =  "id_r" )
			
	)
	private List<Ruolo> ruoli = new ArrayList<>();

	public int getIdUtente() {
		return idUtente;
	}

	public void setIdUtente(int idUtente) {
		this.idUtente = idUtente;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Ruolo> getRuoli() {
		return ruoli;
	}

	public void setRuoli(List<Ruolo> ruoli) {
		this.ruoli = ruoli;
	}
	
	
	
}
