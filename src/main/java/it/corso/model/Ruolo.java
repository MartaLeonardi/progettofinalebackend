package it.corso.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;

@Entity
@Table( name = "ruolo" )
public class Ruolo {

	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "id_r")
	private int idRuolo;
	
	@Column( name = "tipologia" )
	private String tipologia;
	
	@ManyToMany( cascade = CascadeType.REFRESH, fetch = FetchType.EAGER )
	@JoinTable
	(
			name = "utente_ruolo", 
			joinColumns = @JoinColumn( name = "fk_r" , referencedColumnName = "id_r" ),
			inverseJoinColumns = @JoinColumn( name = "fk_u", referencedColumnName =  "id_u" )
			
	)
	private List<Utente> utenti = new ArrayList<>();

	public int getIdRuolo() {
		return idRuolo;
	}

	public void setIdRuolo(int idRuolo) {
		this.idRuolo = idRuolo;
	}

	public String getTipologia() {
		return tipologia;
	}

	public void setTipologia(String tipologia) {
		this.tipologia = tipologia;
	}

	public List<Utente> getUtenti() {
		return utenti;
	}

	public void setUtenti(List<Utente> utenti) {
		this.utenti = utenti;
	}
	
	
}
