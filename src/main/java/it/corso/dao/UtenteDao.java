package it.corso.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import it.corso.model.Utente;

public interface UtenteDao extends CrudRepository<Utente, Integer> {
	
	boolean existsByEmail(String email);
	Utente findByEmailAndPassword(String email, String password);
	Utente findByEmail(String email);
}
