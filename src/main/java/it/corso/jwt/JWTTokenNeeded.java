package it.corso.jwt;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.ws.rs.NameBinding;

@NameBinding // Lega la classe al filtro: saprà che serve il token
@Retention( RetentionPolicy.RUNTIME ) // Specifica che questa annotation sarà disponibile solamente durante il runtime.
@Target( { ElementType.TYPE, ElementType.METHOD } ) // Specifica dove poter applicare l'annotation che stiamo creando: i questo caso sia su classi che su metodi
public @interface JWTTokenNeeded {
	
}
