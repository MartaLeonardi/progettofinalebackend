package it.corso.controller;

import java.security.Key;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import it.corso.dto.UtenteLoginRequestDto;
import it.corso.dto.UtenteLoginResponseDto;
import it.corso.dto.UtenteRegistrazioneDto;
import it.corso.dto.UtenteRuoloDto;
import it.corso.jwt.JWTTokenNeeded;
import it.corso.jwt.Secured;
import it.corso.model.Utente;
import it.corso.service.UtenteService;
import jakarta.validation.Valid;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/utente")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UtenteController {

	@Autowired
	private UtenteService utenteService;
	
	@POST
	@Path("/registrazione")
	public Response userRegistration( @Valid @RequestBody UtenteRegistrazioneDto utenteDto ) {
		
		try {
			
			// Validazione password
			if( !Pattern.matches("(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@.#$!%*?&^])[A-Za-z\\d@.#$!%*?&]{6,20}", utenteDto.getPassword()) ) {
				
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
			
			// Verifico se esiste già l'utente attraverso l'email
			if( utenteService.existsUtenteByEmail(utenteDto.getEmail())) {
				
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
			
			utenteService.userRegistration(utenteDto);
			return Response.status(Response.Status.OK).build();
			
		} catch (Exception e) {
			
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
	}
	
	@POST
	@Path("/login")
	public Response loginUtente(@Valid @RequestBody UtenteLoginRequestDto utente) {
		
		try {
			
			if( utenteService.loginUtente(utente) ) {
				// Se il login è true, le credenziali sono valide e di conseguenza
				// dobbiamo fare tornare il token: lo inserisco nel corpo della risposta
				return Response.ok(issueToken(utente.getEmail())).build();
			}
			
			// Se non passa, ritorno che la richiesta è sbagliata
			return Response.status(Response.Status.BAD_REQUEST).build();
		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
	
	public UtenteLoginResponseDto issueToken(String email) {
			
			// Creo la chiave segreta
			byte[] secret = "stringachiave1234567891111111111111".getBytes();
			
			Key chiaveSegreta = Keys.hmacShaKeyFor(secret);
			
			// Dobbiamo prendere i dati dell'utente
			Utente infoUtente = utenteService.findByEmail(email);
			
			// Devo riprendermi le informazioni di questo utente utilizzando una mappa
			Map<String, Object> map = new HashMap<>();
			
			map.put("nome", infoUtente.getNome());
			map.put("cognome", infoUtente.getCognome());
			map.put("email", email);
			
			List<String> ruoliUtente = new ArrayList<>();
			infoUtente.getRuoli().forEach(r -> ruoliUtente.add(r.getTipologia()));
			
			map.put("ruoli", ruoliUtente);
			
			// Creo la creazione della data
			Date creation = new Date();
			// Crea la data di fine del token utilizzando una classe che ci permette di avere 
			// il timestamp(tempo in millisecondi) + i 15 minuti scelti
			Date end = java.sql.Timestamp.valueOf(LocalDateTime.now().plusMinutes(15L));
			
			// Creazione token
			String jwtToken = Jwts.builder()
								  .setClaims(map)
								  .setIssuer("http://localhost:8080") // Chi emette il token = il server
								  .setIssuedAt(creation) // Data di creazione del token
								  .setExpiration(end) // Data fine validità del token
								  .signWith(chiaveSegreta) // Chiave segreta per firmare
								  .compact();
			
			// Dobbiamo creare l'entità per la risposta
			UtenteLoginResponseDto token = new UtenteLoginResponseDto();
			token.setToken(jwtToken);
			token.setTtl(end);
			token.setTokenCreationTime(creation);
			
			return token;
		}
	
	
	@Secured( role = "Admin" )
	@JWTTokenNeeded 
	@GET
	@Path("/getUtenti")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUtenti() {
		
		try {
			
			List<UtenteRuoloDto> listaUtenti = new ArrayList<>();
			
			
				listaUtenti = utenteService.getUtenti();
			
			
			return Response.status(Response.Status.OK).entity(listaUtenti).build();
			
			
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
}
