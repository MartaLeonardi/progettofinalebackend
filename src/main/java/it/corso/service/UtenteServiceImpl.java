package it.corso.service;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.corso.dao.UtenteDao;
import it.corso.dto.UtenteLoginRequestDto;
import it.corso.dto.UtenteRegistrazioneDto;
import it.corso.dto.UtenteRuoloDto;
import it.corso.model.Utente;

@Service
public class UtenteServiceImpl implements UtenteService {

	@Autowired
	private UtenteDao utenteDao;
	
	private ModelMapper mapper = new ModelMapper();
	
	@Override
	public void userRegistration(UtenteRegistrazioneDto utenteDto) {
		
		Utente utente = new Utente();
		
		//setto i campi inseriti dall'utente
		utente.setNome(utenteDto.getNome());
		utente.setCognome(utenteDto.getCognome());
		utente.setEmail(utenteDto.getEmail());
		
		//si applica l'hashing alla password immessa dall'utente
		String sha256hex = DigestUtils.sha256Hex(utenteDto.getPassword());
		
		//setto il campo della password
		utente.setPassword(sha256hex);
		
		utenteDao.save(utente);

	}
	
	

	@Override
	public boolean existsUtenteByEmail(String email) {
		return utenteDao.existsByEmail(email);
	}



	@Override
	public boolean loginUtente(UtenteLoginRequestDto utenteLogin) {
		
		Utente utente = new Utente();
		
		utente.setEmail(utenteLogin.getEmail());
		utente.setPassword(utenteLogin.getPassword());
		
		String sha256hex = DigestUtils.sha256Hex(utenteLogin.getPassword());
			
		Utente credenzialiUtente = utenteDao.findByEmailAndPassword(utente.getEmail(), sha256hex);
		
		return credenzialiUtente != null ? true : false;
	}



	@Override
	public Utente findByEmail(String email) {
		System.out.println(utenteDao.findByEmail(email));
		return utenteDao.findByEmail(email);
	}

	
	
	@Override
	public List<UtenteRuoloDto> getUtenti() {
		
		List<Utente> utenti = (List<Utente>) utenteDao.findAll();
		
		List<UtenteRuoloDto> utentiDto = new ArrayList<>();
		
		// Facciamo questa lista di dto per evitare i loop di riferimento
		utenti.forEach(u -> utentiDto.add(mapper.map(u, UtenteRuoloDto.class)));
		
		
		return utentiDto;
	}
	

}
