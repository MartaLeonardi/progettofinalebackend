package it.corso.service;

import java.util.List;

import it.corso.dto.UtenteLoginRequestDto;
import it.corso.dto.UtenteRegistrazioneDto;
import it.corso.dto.UtenteRuoloDto;
import it.corso.model.Utente;

public interface UtenteService {
	
	// Metodo per la registrazione dell'utente
	void userRegistration(UtenteRegistrazioneDto utenteDto);
	
	// Metodo per verificare se esiste l'utente tramite email
	boolean existsUtenteByEmail(String email);
	
	// Metodo per ottenere un utente in base all'email
	Utente findByEmail(String email);
	
	// Metodo per effettuare il login
	boolean loginUtente(UtenteLoginRequestDto utente);
	
	// Metodo per ottenere tutti gli utenti (DISPONIBILE SOLO A CHI HA RUOLO ADMIN)
	List<UtenteRuoloDto> getUtenti();
	
}
