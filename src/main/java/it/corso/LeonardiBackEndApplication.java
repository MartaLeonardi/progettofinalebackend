package it.corso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LeonardiBackEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(LeonardiBackEndApplication.class, args);
	}

}
