package it.corso.dto;

public class RuoloDto {
	
	private int idRuolo;
	
	private String tipologia;

	public String getTipologia() {
		return tipologia;
	}

	public void setTipologia(String tipologia) {
		this.tipologia = tipologia;
	}

	public int getIdRuolo() {
		return idRuolo;
	}

	public void setIdRuolo(int idRuolo) {
		this.idRuolo = idRuolo;
	}

	public RuoloDto(int idRuolo, String tipologia) {
		super();
		this.idRuolo = idRuolo;
		this.tipologia = tipologia;
	}
	
	public RuoloDto() {
		super();
	}
}
